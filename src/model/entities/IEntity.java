package model.entities;

public interface IEntity {

    int getMaxX();

    int getMinX();

    int getMaxY();

    int getMinY();

}
