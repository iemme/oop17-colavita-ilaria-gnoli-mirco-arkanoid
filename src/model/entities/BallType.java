package model.entities;

import utility.CollisionUtility;
import utility.CollisionUtility.*;

public enum BallType {
    STANDARD_BALL{
        public void bounce(Ball ball, IEntity ent) {
            if(ent instanceof Wall || ent instanceof Brick) {
                standardBounce(ball, ent);
            } else if(ent instanceof Bar) {
                barBounce(ball, ent);
            }
        }
    },
    FIRE_BALL{
        public void bounce(Ball ball, IEntity ent) {
            if(ent instanceof Wall || (ent instanceof Brick && ent.isIndistruttibile())) {
                standardBounce(ball, ent);
            } else if(ent instanceof Bar) {
                barBounce(ball, ent);
            }
        }
    };
    
    
    private static void barBounce(Ball b, Bar bar) {
        b.setPosition(b.getPosition().getKey(), bar.getMaxY() - b.getRadius()); //ci va? perch no?

        //se cade nel primo terzo rimbalza a sx
        if(b.getPosition().getKey() < bar.getMinX() + bar.getLenght() / 3) {
            b.doOnCollision(PositionOfCollision.RIGHT);
        }
        if(b.getPosition().getKey() > bar.getMinX() + bar.getLenght() / 3 && b.getPosition().getKey() < bar.getMaxX() - bar.getLenght() / 3) {
            b.doOnCollision(PositionOfCollision.DOWN);
        }
        
        if(b.getPosition().getKey() > bar.getMaxX() - bar.getLenght() / 3) {
            b.doOnCollision(PositionOfCollision.LEFT);
        }
    }
    
    private static void standardBounce(Ball b, IEntity e) {
        if(b.dirRight()) {
            if(CollisionUtility.firstCollidedWithLeftestVerticalBound(b, e)) {
                b.setPosition(e.getMinX(), b.getPosition().getValue());
                b.doOnCollision(PositionOfCollision.RIGHT);
            }
        }

        if(b.dirLeft()) {
            if(CollisionUtility.firstCollidedWithRightestVerticalBound(b, e)) {
                b.setPosition(e.getMaxX(), b.getPosition().getValue());
                b.doOnCollision(PositionOfCollision.LEFT);
            }
        }

        if(b.dirUp()) {
            if(CollisionUtility.firstCollidedWithLowerHorizontalBound(b, e)){
                b.setPosition(b.getPosition().getKey(), e.getMaxY());
                b.doOnCollision(PositionOfCollision.UP);
            }
        }

        if(b.dirDown()) {
            if(CollisionUtility.firstCollidedWithTopHorizontalBound(b, e)){
                b.setPosition(b.getPosition().getKey(), e.getMinY());
                b.doOnCollision(PositionOfCollision.DOWN);
            }
        }
    }
}
