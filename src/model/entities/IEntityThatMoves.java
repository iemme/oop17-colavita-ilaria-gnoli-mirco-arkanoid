package model.entities;

public interface IEntityThatMoves extends IEntity {
    void refreshPosition();
}
