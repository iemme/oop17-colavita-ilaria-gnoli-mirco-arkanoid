package model.entities;

import javafx.util.Pair;
import utility.CollisionUtility.PositionOfCollision;

public class Ball implements IEntityThatMoves{
    private static final int DEFAULT_SPEED = 10;
    private static final int DEFAULT_RADIOUS = 7;
    private static final int DEFAULT_ANGLE = 150;
    /*
    private static final int CHANGE_ANGLE = 10;
    private int countCollision;
    */

    private Pair<Integer, Integer> position;
    private int speed;
    private double angle;
    private BallType type;

    /** COSTRUTTORI **/
    public Ball(int x, int y, BallType type) {
        this(x, y, DEFAULT_SPEED, DEFAULT_ANGLE, type);
    }

    public Ball(int x, int y, int speed, double angle, BallType type) {
        this.position = new Pair<>(x, y);
        this.speed = speed;
        this.angle = angle % 360;
        this.type = type;
    }
    
    public Ball(Ball b, int deltaAngle) {
        this(b.getPosition().getKey(), b.getPosition().getValue(), b.getSpeed(), b.getAngle() + deltaAngle, b.getType());
    }

    /** COMPORTAMENTO **/
    @Override
    public void refreshPosition() {
        int newX = (int)(this.getPosition().getKey() + this.speed * Math.cos(Math.toRadians(angle)));
        int newY = (int)(this.getPosition().getValue() + this.speed * Math.sin(Math.toRadians(angle)));
        this.setPosition(newX, newY);
    }
    
    public void doOnCollision(final PositionOfCollision pos) {

        switch (pos) {
        case UP:
            if (this.angle > 270) {
                this.angle = 360 - this.angle;
            } else {
                this.angle = 180 - (this.angle - 180);
            }
            break;
        case DOWN:
            if (this.angle < 90) {
                this.angle = 360 - this.angle;
            } else {
                this.angle = 180 + (180 - this.angle);
            }

            break;
        case RIGHT:
            if (this.angle < 90) {
                this.angle = 180 - this.angle;
            } else {
                this.angle = (360 - this.angle) + 180;
            }
            break;
        case LEFT:
            if (this.angle < 180) {
                this.angle = 180 - this.angle;
            } else {
                this.angle = 360 - (this.angle - 180);
            }
            break;
        default:
            System.out.println("throw new Errore");
        }
    }

    public void setPosition(int newX, int newY) {
        this.position = new Pair<>(newX, newY);
    }
    
    public Pair<Integer, Integer> getPosition() {
        return position;
    }
    
    public int getRadius() {
        return DEFAULT_RADIOUS;
    }

    public int getSpeed() {
        return speed;
    }
    
    public double getAngle() {
        return angle;
    }
    
    public BallType getType() {
        return this.type;
    }

    @Override
    public int getMinX() {
        return this.position.getKey() - getRadius();
    }

    @Override
    public int getMaxX() {
        return this.position.getKey() + getRadius();
    }

    @Override
    public int getMinY() {
        return this.position.getValue() - getRadius();
    }

    @Override
    public int getMaxY() {
        return this.position.getValue() + getRadius();
    }
}

